<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Http\RedirectResponse;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Laravel\Socialite\Facades\Socialite;
use Symfony\Component\HttpFoundation\RedirectResponse as RedirectFromGoogle;

class GoogleLoginController extends Controller
{
    /**
     * @return RedirectFromGoogle|RedirectResponse
     */
    public function redirectToGoogle(): RedirectFromGoogle|RedirectResponse
    {
        return Socialite::driver('google')->redirect();
    }


    /**
     * @return RedirectFromGoogle
     */
    public function handleGoogleCallback(): RedirectFromGoogle
    {
        $googleUser = Socialite::driver('google')->stateless()->user();
        $user = User::where('email', $googleUser->email)->first();
        if(!$user)
        {
            $user = User::create(['name' => $googleUser->name, 'email' => $googleUser->email, 'password' => Hash::make(rand(100000,999999)), 'google_id' => $googleUser->token]);
        }
        Auth::login($user);
        return redirect(RouteServiceProvider::HOME);
    }
}
